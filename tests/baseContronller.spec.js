const base = require("../controllers/baseController.js");
const mockRequest = (body = {}) => ( { body });
const mockResponse = () => {
    const res = {};
    res.json = jest.fn().mockReturnValue(res);
    res.status = jest.fn().mockReturnValue(res);
    return res;
}

describe("baseController.index", () => {
    test(`res.json called with { status: true, message: "hello word" }`, done => {
        const req = mockRequest();
        const res = mockResponse();

        base.index(req, res);
        expect(res.status).toBeCalledWith(200);
        expect(res.json).toBeCalledWith({
            status: true,
            message: "hello world",
            name: "zaky"
        })
        done()
    })
})

describe("baseController.sum", () => {
    test("res.json called with {value: 10}", done => {
        const panjang = 10;
        const lebar = 5;
        const req = mockRequest({panjang, lebar});
        const res = mockResponse();
        const luas = panjang * lebar;

        base.sum(req, res);
        expect(res.json).toBeCalledWith({
            value: luas
        })
        done();
    })
})

describe("baseController.grade", () => {
    test("if nilai > 80, grade = A", done => {
        const nilai = 90;
        const kelas = 1;
        const req = mockRequest({nilai});
        const res = mockResponse();

        base.grade(req, res);
        expect(res.json).toBeCalledWith({
            grade: "A"
        })
        done()
    })
    test("if nilai < 80, grade = B", done => {
        const nilai = 70;
        const req = mockRequest({nilai});
        const res = mockResponse();

        base.grade(req, res);
        expect(res.json).toBeCalledWith({
            grade: "B"
        })
        done()
    })
})