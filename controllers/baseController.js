module.exports = {
  index: (req, res) => {
    res.status(200).json({
      status: true,
      message: "hello world",
      name: "zaky",
    });
  },
  sum: (req, res) => {
      // panjang 10, lebar 5 = luas 50
    const panjang = req.body.panjang;
    const lebar = req.body.lebar;
    const luas = panjang * lebar;
    res.status(200).json({
      value: luas,
    });
  },
  grade: (req, res) => {
      const nilai = req.body.nilai;
      let grade = "A";
      if(nilai < 80) grade = "B";

      res.status(200).json({
          grade
      })
  }
};
